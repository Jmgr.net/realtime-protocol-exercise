package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"server/numbers"
	"server/numbersserver"

	"github.com/jessevdk/go-flags"
	grpc "google.golang.org/grpc"
)

// Program options
type options struct {
	Port                 uint16 `short:"p" long:"port" description:"Port to listen to (required)" required:"true"`
	SimulateNetworkError bool   `short:"s" long:"simulateNetworkError" description:"Simulate a network error during each stream"`
	InitialNumber        int32  `short:"i" long:"initialNumber" description:"The initial number to send, instead of generating it randomly (stateless only)"`
}

func main() {
	// Parse command line options
	opts := options{}
	_, err := flags.ParseArgs(&opts, os.Args)
	if err != nil {
		os.Exit(1)
	}

	// Start listening
	listener, err := net.Listen("tcp", fmt.Sprintf("localhost:%v", opts.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create the number server, the gRPC server and start serving numbers
	server := numbersserver.NewNumbersServer(opts.SimulateNetworkError, opts.InitialNumber)
	grpcServer := grpc.NewServer()

	numbers.RegisterNumbersServer(grpcServer, server)

	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
