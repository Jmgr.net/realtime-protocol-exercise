#!/bin/bash

function stateless_no_error() {
    (./server -p5000 -i42 &)

    CLIENT_OUTPUT=$(node ./statelessClient.js -p5000 -n3 2>&1)

    EXPECTED="294"
    if [[ $CLIENT_OUTPUT = $EXPECTED ]]; then
        echo "Success"
    else
        echo "Failure, output is $CLIENT_OUTPUT" 
    fi

    killall server
}

function stateless_network_error() {
    (./server -p5000 -i42 -s &)

    CLIENT_OUTPUT=$(node ./statelessClient.js -p5000 -n3 2>&1)

    EXPECTED="Server error: Error: 2 UNKNOWN: Simulated error
294"
    if [[ $CLIENT_OUTPUT = $EXPECTED ]]; then
        echo "Success"
    else
        echo "Failure, output is $CLIENT_OUTPUT" 
    fi

    killall server
}

function stateful_no_error() {
    (./server -p5000 &)

    CLIENT_OUTPUT=$(node --no-warnings ./statefulClient.js -p5000 -n42 --uuid=baa72925-c9a0-4664-b8e3-248a9ab3269f 2>&1)

    EXPECTED="Checksum: 952783440 => success"
    if [[ $CLIENT_OUTPUT = $EXPECTED ]]; then
        echo "Success"
    else
        echo "Failure, output is $CLIENT_OUTPUT" 
    fi

    killall server
}

function stateful_network_error() {
    (./server -p5000 -s &)

    CLIENT_OUTPUT=$(node --no-warnings ./statefulClient.js -p5000 -n42 --uuid=baa72925-c9a0-4664-b8e3-248a9ab3269f 2>&1)

    EXPECTED="Server error: Error: 2 UNKNOWN: Simulated error
Checksum: 4198270559 => success"
    # Note that the checksum is different here than in stateful_no_error()
    # This is because the server sends more numbers if there is an error
    # so the checksum will be different in the end
    if [[ $CLIENT_OUTPUT = $EXPECTED ]]; then
        echo "Success"
    else
        echo "Failure, output is $CLIENT_OUTPUT" 
    fi

    killall server
}

echo "Stateless"
echo "Test: no error"
stateless_no_error

echo "Test: network error"
stateless_network_error

echo "Stateful"
echo "Test: no error"
stateful_no_error

echo "Test: network error"
stateful_network_error