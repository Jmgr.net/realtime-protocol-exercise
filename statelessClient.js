'use strict';

// Dependencies
const PROTO_PATH = __dirname + '/numbers/numbers.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const minimist = require('minimist')

// Check parameters
const args = minimist(process.argv.slice(2), {
    int: ['port', 'numberCount'],
    alias: { p: 'port', n: 'numberCount' }
  })

if(args.port == undefined) {
    console.error("Missing port parameter")
    process.exit(1)
}

if(args.numberCount == undefined) {
    console.error("Missing number count")
    process.exit(1)
}
if(args.numberCount <= 0 || args.numberCount > 0xffff) {
    console.error("Invalid number count")
    process.exit(1)
}

// Suggested options for similarity to existing grpc.load behavior
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
// The protoDescriptor object has the full package hierarchy
var numbers = protoDescriptor.numbers;

// A sleep function
let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

var receivedNumbersCount = 0
var latestReceivedNumber = 0
var total = 0

function receive() {
    let client = new numbers.Numbers('localhost:' + args.port, grpc.credentials.createInsecure());
    let call = client.statelessGetNumbers({latestReceivedNumber: latestReceivedNumber});

    call.on('data', function(number) {
        latestReceivedNumber = number.number
        total += number.number
        ++receivedNumbersCount

        // Check if we have received enough numbers
        if(receivedNumbersCount >= args.numberCount)
        {
            console.log(total)
            call.cancel()
        }
    });
    call.on('error', async function(e) {
        if(e.code != 1) { // Canceled
            // In case of an error, retry after one second
            console.log("Server error: " + e)
            call.cancel()
            client.close()
            await sleep(1000)
            receive()
        }
    });
}

function main() {
    receive()
}

main()