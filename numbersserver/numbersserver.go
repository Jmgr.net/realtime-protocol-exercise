package numbersserver

import (
	"encoding/binary"
	"errors"
	"hash/crc32"
	"log"
	"math/rand"
	"regexp"
	"server/numbers"
	"server/storage"
	"sync"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Since Go's maps are not concurrency safe we have to use a mutex
// Note that a concurrency-safe map such as https://github.com/orcaman/concurrent-map
// could also have been used here.
// Also note that this map could (and probably should) be persisted
type concurrencySafeUsedUUIDS struct {
	sync.RWMutex
	m map[string]struct{}
}

// NumbersServer represents our server sending numbers
type NumbersServer struct {
	// The session state storage provider
	storageProvider storage.StorageProvider
	// This map is used to store used UUIDs, so that future clients trying to use the same
	// UUIDS will get a proper error message
	usedUUIDS            concurrencySafeUsedUUIDS
	simulateNetworkError bool
	initialNumber        int32
}

// NewNumbersServer creates a new numbers server
func NewNumbersServer(simulateNetworkError bool, initialNumber int32) *NumbersServer {
	server := &NumbersServer{
		storageProvider: storage.NewMemoryStorageProvider(),
		usedUUIDS: concurrencySafeUsedUUIDS{
			m: make(map[string]struct{}),
		},
		simulateNetworkError: simulateNetworkError,
		initialNumber:        initialNumber,
	}

	// Prune states every second
	go func() {
		for {
			<-time.After(1 * time.Second)
			err := server.pruneStates()
			if err != nil {
				log.Printf("Error while pruning states: %v\n", err)
			}
		}
	}()

	return server
}

// generateRandomNumber returns a number between minimum and maximum, both inclusive
func generateRandomNumber(minimum int32, maximum int32) int32 {
	return rand.Int31n(maximum-minimum+1) + minimum
}

// generateRandomNumberFromRand returns a number between minimum and maximum, both inclusive, using a specified random generator
func generateRandomNumberFromRand(minimum int32, maximum int32, customRand *rand.Rand) int32 {
	return customRand.Int31n(maximum-minimum+1) + minimum
}

// StatelessGetNumbers sends numbers in a stateless way
func (s *NumbersServer) StatelessGetNumbers(params *numbers.StatelessParameters, stream numbers.Numbers_StatelessGetNumbersServer) error {
	var number int32
	if params.LatestReceivedNumber > 0 {
		// Resumed number sequence
		number = params.LatestReceivedNumber * 2
	} else {
		// Initial sequence
		if s.initialNumber != 0 {
			number = s.initialNumber
		} else {
			number = generateRandomNumber(1, 0xff)
		}
	}

	for {
		// Simulate a network error by returning an error from time to time
		if s.simulateNetworkError && generateRandomNumber(1, 3) == 1 {
			return errors.New("Simulated error")
		}

		err := stream.Send(&numbers.StatelessNumber{Number: number})
		if err != nil {
			return status.Error(codes.Internal, "Internal error: "+err.Error())
		}

		number *= 2

		time.Sleep(1 * time.Second)
	}
}

// computeChecksum computes a CRC32 checksum using a number and the current checksum
func computeChecksum(number uint32, previousChecksum uint32) uint32 {
	binaryNumber := make([]byte, 8)
	binary.BigEndian.PutUint32(binaryNumber, number)
	binary.BigEndian.PutUint32(binaryNumber[4:], previousChecksum)

	return crc32.ChecksumIEEE(binaryNumber)
}

const sequenceSize = 3

// https://stackoverflow.com/questions/25051675/how-to-validate-uuid-v4-in-go
// isValidUUID validates a UUID
func isValidUUID(uuid string) bool {
	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")
	return r.MatchString(uuid)
}

// StatefulGetNumbers sends numbers in a stateful way
func (s *NumbersServer) StatefulGetNumbers(params *numbers.StatefulParameters, stream numbers.Numbers_StatefulGetNumbersServer) error {
	// Check input parameters
	if !isValidUUID(params.Uuid) {
		return status.Error(codes.InvalidArgument, "Invalid UUID")
	}
	if params.RandomNumber > 0xffff {
		return status.Error(codes.InvalidArgument, "Invalid random number (has to be < 0xffff)")
	}

	// Check if the UUID has been used previously
	s.usedUUIDS.RLock()
	_, knownUsedUUID := s.usedUUIDS.m[params.Uuid]
	s.usedUUIDS.RUnlock()
	if knownUsedUUID {
		return status.Error(codes.DeadlineExceeded, "This UUID has already been used")
	}

	// Check if we have a session for this client
	hasEntry, err := s.storageProvider.HasEntry(params.Uuid)
	if err != nil {
		return status.Error(codes.Internal, "Internal error: "+err.Error())
	}

	// Fetch the stored session data, or create it otherwise
	var storageEntry storage.StorageEntry
	if hasEntry {
		storageEntry, err = s.storageProvider.GetEntry(params.Uuid)
		if err != nil {
			return status.Error(codes.Internal, "Internal error: "+err.Error())
		}
	} else {
		storageEntry = storage.NewStorageEntry(params.RandomNumber)
		err = s.storageProvider.SetEntry(params.Uuid, storageEntry)
		if err != nil {
			return status.Error(codes.Internal, "Internal error: "+err.Error())
		}
	}

	sentNumberCount := 0

	for {
		var number uint32

		if storageEntry.LatestNumber != 0 {
			number = storageEntry.LatestNumber
		} else {
			number = storageEntry.Rand.Uint32()

			// Store the latest number in case there is an error
			storageEntry.LatestNumber = number
			err = s.storageProvider.SetEntry(params.Uuid, storageEntry)
			if err != nil {
				return status.Error(codes.Internal, "Internal error: "+err.Error())
			}
		}

		// Update the checksum
		checksum := computeChecksum(number, storageEntry.Checksum)

		statefulNumber := &numbers.StatefulNumber{Number: number}

		// Send the checksum with the last number
		if sentNumberCount == sequenceSize-1 {
			statefulNumber.Checksum = checksum
		}

		// Simulate a network error
		if s.simulateNetworkError && sentNumberCount == 2 && !storageEntry.SimulatedNetworkError {
			storageEntry.SimulatedNetworkError = true
			err = s.storageProvider.SetEntry(params.Uuid, storageEntry)
			if err != nil {
				return status.Error(codes.Internal, "Internal error: "+err.Error())
			}
			return errors.New("Simulated error")
		}

		err := stream.Send(statefulNumber)
		if err != nil {
			return status.Error(codes.Internal, "Internal error: "+err.Error())
		}

		// Only update the session data if we have successfully sent the number
		storageEntry.Checksum = checksum
		storageEntry.LatestNumber = 0
		sentNumberCount++

		err = s.storageProvider.SetEntry(params.Uuid, storageEntry)
		if err != nil {
			return status.Error(codes.Internal, "Internal error: "+err.Error())
		}

		if sentNumberCount >= sequenceSize {
			break
		}

		time.Sleep(1 * time.Second)
	}

	// Remove the session data if it has finished correctly
	s.storageProvider.RemoveEntry(params.Uuid)

	// Mark the UUID has used
	s.usedUUIDS.Lock()
	s.usedUUIDS.m[params.Uuid] = struct{}{}
	s.usedUUIDS.Unlock()

	return nil
}

// pruneStates removes session data entries older than 30 seconds (since the last number was sent)
func (s *NumbersServer) pruneStates() error {
	removedEntries, err := s.storageProvider.PruneEntries(30 * time.Second)
	if err != nil {
		return err
	}

	s.usedUUIDS.Lock()
	for _, removedEntry := range removedEntries {
		s.usedUUIDS.m[removedEntry] = struct{}{}
	}
	s.usedUUIDS.Unlock()

	return nil
}
