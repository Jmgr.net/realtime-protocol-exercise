'use strict';

// Dependencies
const PROTO_PATH = __dirname + '/numbers/numbers.proto';
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const uuidv4 = require('uuid/v4')
const crc = require('node-crc');
const minimist = require('minimist')

// Check parameters
const args = minimist(process.argv.slice(2), {
    int: ['port', 'randomNumber'],
    string: ['uuid'],
    alias: { p: 'port', n: 'randomNumber' }
  })

if(args.port == undefined) {
    console.error("Missing port parameter")
    process.exit(1)
}

// Suggested options for similarity to existing grpc.load behavior
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var protoDescriptor = grpc.loadPackageDefinition(packageDefinition);
// The protoDescriptor object has the full package hierarchy
var numbers = protoDescriptor.numbers;

// A sleep function
let sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

// Generate a uuid for this execution, or use the provided one
var uuid
if(args.uuid == undefined) {
    uuid = uuidv4()
} else {
    uuid = args.uuid
}

// Generate a random number for this execution, or use the provided one
var randomNumber
if(args.randomNumber == undefined) {
    randomNumber = randomInt(0, 0xffff)
} else {
    randomNumber = args.randomNumber
}

var currentChecksum = 0
var latestNumber

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}

function receive() {
    let client = new numbers.Numbers('localhost:' + args.port, grpc.credentials.createInsecure());
    let call = client.statefulGetNumbers({
        uuid: uuid,
        randomNumber: randomNumber,
    });

    call.on('data', function(number) {
        // Update the checksum
        let buffer = Buffer.allocUnsafe(8)
        buffer.writeUInt32BE(number.number, 0);
        buffer.writeUInt32BE(currentChecksum, 4);
        let checksum = crc.crc32(buffer)
        currentChecksum = checksum.readUInt32BE()

        // Save the latest number
        latestNumber = number
    });
    call.on('end', function() {
        if(latestNumber.checksum == currentChecksum) {
            console.log("Checksum: " + latestNumber.checksum + " => success")
        } else {
            console.log("Checksum: server: " + latestNumber.checksum + " client: " + currentChecksum + " => failure")
        }
    });
    call.on('error', async function(e) {
        // In case of an error, retry after one second
        console.log("Server error: " + e)
        call.cancel()
        client.close()
        await sleep(1000)
        receive()
    });
}

function main() {
    receive()
}

main()