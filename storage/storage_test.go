package storage

import (
	"math/rand"
	"testing"
	"time"
)

const firstUUID = "17f3304e-5b87-41a3-b789-d26ab69e869c"
const secondUUID = "07ee6070-6f36-4289-803e-7da069104c85"

func createStorageProvider() StorageProvider {
	// Return a memory storage provider now, but could another type of storage provider
	return NewMemoryStorageProvider()
}

func checkHasEntry(t *testing.T, storageProvider StorageProvider, UUID string, expected bool) {
	hasEntry, err := storageProvider.HasEntry(UUID)
	if err != nil {
		t.Errorf("error while calling HasEntry: %v", err)
	}
	if hasEntry != expected {
		t.Errorf("HasEntry returned %v, expected %v", hasEntry, expected)
	}
}

func TestMemoryStorage_AddAndHasEntry(t *testing.T) {
	storageProvider := createStorageProvider()

	entryInput := NewStorageEntry(42)
	entryInput.Checksum = uint32(rand.Int31())
	err := storageProvider.SetEntry(firstUUID, entryInput)
	if err != nil {
		t.Errorf("error while calling SetEntry: %v", err)
	}

	checkHasEntry(t, storageProvider, firstUUID, true)
	checkHasEntry(t, storageProvider, secondUUID, false)
}

func TestMemoryStorage_AddAndGetEntry(t *testing.T) {
	storageProvider := createStorageProvider()

	entryInput := NewStorageEntry(42)
	entryInput.Checksum = uint32(rand.Int31())
	err := storageProvider.SetEntry(firstUUID, entryInput)
	if err != nil {
		t.Errorf("error while calling SetEntry: %v", err)
	}

	entryOutput, err := storageProvider.GetEntry(firstUUID)
	if err != nil {
		t.Errorf("error while calling GetEntry: %v", err)
	}
	if entryOutput.Checksum != entryInput.Checksum {
		t.Errorf("CurrentNumberCount has an incorrect value, expected %v, got %v",
			entryInput.Checksum, entryOutput.Checksum)
	}

	entryOutput, err = storageProvider.GetEntry(secondUUID)
	if err == nil {
		t.Error("GetEntry on a non-existent entry returned a nil error")
	}
}

func TestMemoryStorage_AddAndRemoveEntry(t *testing.T) {
	storageProvider := createStorageProvider()

	entryInput := NewStorageEntry(42)
	entryInput.Checksum = uint32(rand.Int31())
	err := storageProvider.SetEntry(firstUUID, entryInput)
	if err != nil {
		t.Errorf("error while calling SetEntry: %v", err)
	}

	err = storageProvider.RemoveEntry(firstUUID)
	if err != nil {
		t.Errorf("error while calling RemoveEntry: %v", err)
	}

	checkHasEntry(t, storageProvider, firstUUID, false)
}

func TestMemoryStorage_Pruning(t *testing.T) {
	storageProvider := createStorageProvider()

	err := storageProvider.SetEntry(firstUUID, NewStorageEntry(42))
	if err != nil {
		t.Errorf("error while calling SetEntry: %v", err)
	}

	time.Sleep(1 * time.Second)

	err = storageProvider.SetEntry(secondUUID, NewStorageEntry(42))
	if err != nil {
		t.Errorf("error while calling SetEntry: %v", err)
	}

	time.Sleep(1 * time.Second)

	entries, err := storageProvider.PruneEntries(2 * time.Second)
	if err != nil {
		t.Errorf("error while calling PruneEntries: %v", err)
	}
	if len(entries) != 1 || entries[0] != firstUUID {
		t.Error("failed pruning entries")
	}

	checkHasEntry(t, storageProvider, firstUUID, false)
	checkHasEntry(t, storageProvider, secondUUID, true)
}
