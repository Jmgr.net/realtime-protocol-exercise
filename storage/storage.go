package storage

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// StorageEntry represents an entry in the storage
type StorageEntry struct {
	// The last time data has been sent
	LastUpdateTime time.Time
	// The random number generator
	Rand *rand.Rand
	// The current checksum
	Checksum uint32
	// The current number, used when an error occurs so that the last number gets delivered again
	LatestNumber uint32
	// Has a network error been simulated during this session
	SimulatedNetworkError bool
}

// NewStorageEntry takes a random generator seed and returns a new storage entry
func NewStorageEntry(randomGeneratorSeed int32) StorageEntry {
	return StorageEntry{
		LastUpdateTime: time.Now(),
		Rand:           rand.New(rand.NewSource(int64(randomGeneratorSeed))),
	}
}

// StorageProvider is an interface for storage providers
// Note that it could have been more generic using interface{} as the entry type
// and "key" instead of "uuid"
type StorageProvider interface {
	HasEntry(uuid string) (bool, error)
	GetEntry(uuid string) (StorageEntry, error)
	SetEntry(uuid string, entry StorageEntry) error
	RemoveEntry(uuid string) error
	PruneEntries(maximumDuration time.Duration) ([]string, error)
}

// Since Go's maps are not concurrency safe we have to use a mutex
// Note that a concurrency-safe map such as https://github.com/orcaman/concurrent-map
// could also have been used here.
type concurrencySafeMemoryStorage struct {
	sync.RWMutex
	m map[string]StorageEntry
}

// MemoryStorageProvider represents a memory storage provider
type MemoryStorageProvider struct {
	storage concurrencySafeMemoryStorage
}

// NewMemoryStorageProvider returns a new memory storage provider
func NewMemoryStorageProvider() *MemoryStorageProvider {
	storageProvider := &MemoryStorageProvider{}
	storageProvider.storage.m = make(map[string]StorageEntry)

	return storageProvider
}

// HasEntry returns true if an entry exists within the storage
func (sp *MemoryStorageProvider) HasEntry(uuid string) (bool, error) {
	sp.storage.RLock()
	_, ok := sp.storage.m[uuid]
	sp.storage.RUnlock()

	return ok, nil
}

// GetEntry returns an entry's value
func (sp *MemoryStorageProvider) GetEntry(uuid string) (StorageEntry, error) {
	sp.storage.RLock()
	entry, ok := sp.storage.m[uuid]
	sp.storage.RUnlock()

	if !ok {
		return StorageEntry{}, fmt.Errorf("entry %v not found", uuid)
	}

	return entry, nil
}

// SetEntry sets an entry's value
func (sp *MemoryStorageProvider) SetEntry(uuid string, entry StorageEntry) error {
	entry.LastUpdateTime = time.Now()

	sp.storage.Lock()
	sp.storage.m[uuid] = entry
	sp.storage.Unlock()

	return nil
}

// RemoveEntry removes an entry from the storage; does nothing if the entry doesn't exist
func (sp *MemoryStorageProvider) RemoveEntry(uuid string) error {
	sp.storage.Lock()
	delete(sp.storage.m, uuid)
	sp.storage.Unlock()

	return nil
}

// PruneEntries removes entries that are older than a maximum time, and returns an array of the uuid that have been removed
func (sp *MemoryStorageProvider) PruneEntries(maximumTime time.Duration) ([]string, error) {
	removedEntries := make([]string, 0)

	sp.storage.Lock()
	for key, value := range sp.storage.m {
		if time.Since(value.LastUpdateTime) > maximumTime {
			delete(sp.storage.m, key)
			removedEntries = append(removedEntries, key)
		}
	}
	sp.storage.Unlock()

	return removedEntries, nil
}
